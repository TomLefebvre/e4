﻿namespace PPE_4_CS
{
    partial class AffectationVoeux
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label_v_point = new System.Windows.Forms.Label();
            this.label_v_nom = new System.Windows.Forms.Label();
            this.label_v_prenom = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_refuser_3 = new System.Windows.Forms.Button();
            this.btn_refuser_2 = new System.Windows.Forms.Button();
            this.btn_accepter_3 = new System.Windows.Forms.Button();
            this.btn_accepter_2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_refuser = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_accepter = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label_ARA = new System.Windows.Forms.Label();
            this.label_BFC = new System.Windows.Forms.Label();
            this.label_Bre = new System.Windows.Forms.Label();
            this.label_CVL = new System.Windows.Forms.Label();
            this.label_Cor = new System.Windows.Forms.Label();
            this.label_GrE = new System.Windows.Forms.Label();
            this.label_HdF = new System.Windows.Forms.Label();
            this.label_IdF = new System.Windows.Forms.Label();
            this.label_Nor = new System.Windows.Forms.Label();
            this.label_NAq = new System.Windows.Forms.Label();
            this.label_Occ = new System.Windows.Forms.Label();
            this.label_PdL = new System.Windows.Forms.Label();
            this.label_PACA = new System.Windows.Forms.Label();
            this.label_Gua = new System.Windows.Forms.Label();
            this.label_Guy = new System.Windows.Forms.Label();
            this.label_Mar = new System.Windows.Forms.Label();
            this.label_Reu = new System.Windows.Forms.Label();
            this.label_May = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_quiter = new System.Windows.Forms.Button();
            this.btn_deconnexion = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.lbl_filtre = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Controls.Add(this.label11);
            this.panel1.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1044, 55);
            this.panel1.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label11.Location = new System.Drawing.Point(378, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(410, 46);
            this.label11.TabIndex = 0;
            this.label11.Text = "Galaxy Swiss Bourdin";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(184, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(334, 21);
            this.comboBox1.TabIndex = 8;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel5.Controls.Add(this.label_v_point);
            this.panel5.Controls.Add(this.label_v_nom);
            this.panel5.Controls.Add(this.label_v_prenom);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.tableLayoutPanel3);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Controls.Add(this.comboBox1);
            this.panel5.Location = new System.Drawing.Point(443, 96);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(613, 231);
            this.panel5.TabIndex = 24;
            // 
            // label_v_point
            // 
            this.label_v_point.AutoSize = true;
            this.label_v_point.Location = new System.Drawing.Point(458, 203);
            this.label_v_point.Name = "label_v_point";
            this.label_v_point.Size = new System.Drawing.Size(49, 13);
            this.label_v_point.TabIndex = 35;
            this.label_v_point.Text = "Prenom :";
            // 
            // label_v_nom
            // 
            this.label_v_nom.AutoSize = true;
            this.label_v_nom.Location = new System.Drawing.Point(284, 203);
            this.label_v_nom.Name = "label_v_nom";
            this.label_v_nom.Size = new System.Drawing.Size(49, 13);
            this.label_v_nom.TabIndex = 34;
            this.label_v_nom.Text = "Prenom :";
            // 
            // label_v_prenom
            // 
            this.label_v_prenom.AutoSize = true;
            this.label_v_prenom.Location = new System.Drawing.Point(109, 203);
            this.label_v_prenom.Name = "label_v_prenom";
            this.label_v_prenom.Size = new System.Drawing.Size(49, 13);
            this.label_v_prenom.TabIndex = 33;
            this.label_v_prenom.Text = "Prenom :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(392, 203);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Points  :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(234, 203);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "Nom :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(54, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Prenom :";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble;
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 87F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 102F));
            this.tableLayoutPanel3.Controls.Add(this.btn_refuser_3, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.btn_refuser_2, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.btn_accepter_3, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.btn_accepter_2, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.btn_refuser, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.label9, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.btn_accepter, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.label10, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label12, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label13, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label14, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label15, 1, 3);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(98, 40);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26.92308F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.59615F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.24038F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.24038F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(420, 154);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // btn_refuser_3
            // 
            this.btn_refuser_3.BackColor = System.Drawing.Color.DarkRed;
            this.btn_refuser_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_refuser_3.Font = new System.Drawing.Font("Liberation Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_refuser_3.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_refuser_3.Location = new System.Drawing.Point(318, 118);
            this.btn_refuser_3.Name = "btn_refuser_3";
            this.btn_refuser_3.Size = new System.Drawing.Size(80, 25);
            this.btn_refuser_3.TabIndex = 30;
            this.btn_refuser_3.Text = "Refuser";
            this.btn_refuser_3.UseVisualStyleBackColor = false;
            this.btn_refuser_3.Click += new System.EventHandler(this.btn_refuser_3_Click);
            // 
            // btn_refuser_2
            // 
            this.btn_refuser_2.BackColor = System.Drawing.Color.DarkRed;
            this.btn_refuser_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_refuser_2.Font = new System.Drawing.Font("Liberation Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_refuser_2.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_refuser_2.Location = new System.Drawing.Point(318, 80);
            this.btn_refuser_2.Name = "btn_refuser_2";
            this.btn_refuser_2.Size = new System.Drawing.Size(80, 25);
            this.btn_refuser_2.TabIndex = 30;
            this.btn_refuser_2.Text = "Refuser";
            this.btn_refuser_2.UseVisualStyleBackColor = false;
            this.btn_refuser_2.Click += new System.EventHandler(this.btn_refuser_2_Click);
            // 
            // btn_accepter_3
            // 
            this.btn_accepter_3.BackColor = System.Drawing.Color.Green;
            this.btn_accepter_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_accepter_3.Font = new System.Drawing.Font("Liberation Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_accepter_3.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_accepter_3.Location = new System.Drawing.Point(228, 118);
            this.btn_accepter_3.Name = "btn_accepter_3";
            this.btn_accepter_3.Size = new System.Drawing.Size(80, 26);
            this.btn_accepter_3.TabIndex = 30;
            this.btn_accepter_3.Text = "Accepter";
            this.btn_accepter_3.UseVisualStyleBackColor = false;
            this.btn_accepter_3.Click += new System.EventHandler(this.btn_accepter_3_Click);
            // 
            // btn_accepter_2
            // 
            this.btn_accepter_2.BackColor = System.Drawing.Color.Green;
            this.btn_accepter_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_accepter_2.Font = new System.Drawing.Font("Liberation Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_accepter_2.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_accepter_2.Location = new System.Drawing.Point(228, 80);
            this.btn_accepter_2.Name = "btn_accepter_2";
            this.btn_accepter_2.Size = new System.Drawing.Size(80, 26);
            this.btn_accepter_2.TabIndex = 30;
            this.btn_accepter_2.Text = "Accepter";
            this.btn_accepter_2.UseVisualStyleBackColor = false;
            this.btn_accepter_2.Click += new System.EventHandler(this.btn_accepter_2_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Nom de la région";
            // 
            // btn_refuser
            // 
            this.btn_refuser.BackColor = System.Drawing.Color.DarkRed;
            this.btn_refuser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_refuser.Font = new System.Drawing.Font("Liberation Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_refuser.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_refuser.Location = new System.Drawing.Point(318, 46);
            this.btn_refuser.Name = "btn_refuser";
            this.btn_refuser.Size = new System.Drawing.Size(80, 25);
            this.btn_refuser.TabIndex = 28;
            this.btn_refuser.Text = "Refuser";
            this.btn_refuser.UseVisualStyleBackColor = false;
            this.btn_refuser.Click += new System.EventHandler(this.btn_refuser_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(185, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Rang";
            // 
            // btn_accepter
            // 
            this.btn_accepter.BackColor = System.Drawing.Color.Green;
            this.btn_accepter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_accepter.Font = new System.Drawing.Font("Liberation Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_accepter.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_accepter.Location = new System.Drawing.Point(228, 46);
            this.btn_accepter.Name = "btn_accepter";
            this.btn_accepter.Size = new System.Drawing.Size(80, 25);
            this.btn_accepter.TabIndex = 27;
            this.btn_accepter.Text = "Accepter";
            this.btn_accepter.UseVisualStyleBackColor = false;
            this.btn_accepter.Click += new System.EventHandler(this.btn_accepter_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 43);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Non attribué";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Non attribué";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 115);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Non attribué";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(193, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 20);
            this.label13.TabIndex = 9;
            this.label13.Text = "1";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(193, 84);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(18, 20);
            this.label14.TabIndex = 10;
            this.label14.Text = "2";
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(193, 123);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(18, 20);
            this.label15.TabIndex = 11;
            this.label15.Text = "3";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(101, 15);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 18);
            this.label16.TabIndex = 0;
            this.label16.Text = "Visiteur : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(590, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(290, 18);
            this.label2.TabIndex = 25;
            this.label2.Text = "Visiteurs ayant fait une demande de voeux :";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetPartial;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 79.941F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.059F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 102F));
            this.tableLayoutPanel1.Controls.Add(this.button18, 3, 17);
            this.tableLayoutPanel1.Controls.Add(this.button17, 3, 16);
            this.tableLayoutPanel1.Controls.Add(this.button16, 3, 15);
            this.tableLayoutPanel1.Controls.Add(this.button15, 3, 14);
            this.tableLayoutPanel1.Controls.Add(this.button14, 3, 13);
            this.tableLayoutPanel1.Controls.Add(this.button13, 3, 12);
            this.tableLayoutPanel1.Controls.Add(this.button12, 3, 10);
            this.tableLayoutPanel1.Controls.Add(this.button10, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.button9, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.button8, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.button7, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.button6, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.button5, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.button4, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.button3, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.button2, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.button1, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox18, 2, 17);
            this.tableLayoutPanel1.Controls.Add(this.textBox17, 2, 16);
            this.tableLayoutPanel1.Controls.Add(this.textBox16, 2, 15);
            this.tableLayoutPanel1.Controls.Add(this.textBox15, 2, 14);
            this.tableLayoutPanel1.Controls.Add(this.textBox14, 2, 13);
            this.tableLayoutPanel1.Controls.Add(this.textBox13, 2, 12);
            this.tableLayoutPanel1.Controls.Add(this.textBox12, 2, 11);
            this.tableLayoutPanel1.Controls.Add(this.textBox11, 2, 10);
            this.tableLayoutPanel1.Controls.Add(this.textBox10, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.textBox9, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.textBox8, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.textBox7, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.textBox6, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBox5, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBox4, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.textBox3, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox2, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label22, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label_ARA, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label_BFC, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label_Bre, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label_CVL, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label_Cor, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label_GrE, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label_HdF, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label_IdF, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label_Nor, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.label_NAq, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.label_Occ, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.label_PdL, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.label_PACA, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.label_Gua, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.label_Guy, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.label_Mar, 1, 15);
            this.tableLayoutPanel1.Controls.Add(this.label_Reu, 1, 16);
            this.tableLayoutPanel1.Controls.Add(this.label_May, 1, 17);
            this.tableLayoutPanel1.Controls.Add(this.label23, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label24, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label25, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label26, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label27, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label28, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label29, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label30, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label31, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label32, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.label33, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.label34, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.label35, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.label36, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.label37, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this.label38, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this.label39, 0, 17);
            this.tableLayoutPanel1.Controls.Add(this.button11, 3, 11);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 95);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel1.RowCount = 18;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(425, 596);
            this.tableLayoutPanel1.TabIndex = 26;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(321, 568);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(75, 23);
            this.button18.TabIndex = 31;
            this.button18.Text = "Modifier";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(321, 535);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(75, 23);
            this.button17.TabIndex = 31;
            this.button17.Text = "Modifier";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(321, 502);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 23);
            this.button16.TabIndex = 31;
            this.button16.Text = "Modifier";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(321, 469);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(75, 23);
            this.button15.TabIndex = 31;
            this.button15.Text = "Modifier";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(321, 436);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 23);
            this.button14.TabIndex = 31;
            this.button14.Text = "Modifier";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(321, 403);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 23);
            this.button13.TabIndex = 31;
            this.button13.Text = "Modifier";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(321, 337);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 31;
            this.button12.Text = "Modifier";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(321, 304);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 31;
            this.button10.Text = "Modifier";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(321, 271);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 31;
            this.button9.Text = "Modifier";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(321, 238);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 31;
            this.button8.Text = "Modifier";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(321, 205);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 31;
            this.button7.Text = "Modifier";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(321, 172);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 31;
            this.button6.Text = "Modifier";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(321, 139);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 31;
            this.button5.Text = "Modifier";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(321, 106);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 31;
            this.button4.Text = "Modifier";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(321, 73);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 31;
            this.button3.Text = "Modifier";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(321, 40);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 31;
            this.button2.Text = "Modifier";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(321, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 30;
            this.button1.Text = "Modifier";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(248, 568);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(64, 26);
            this.textBox18.TabIndex = 31;
            this.textBox18.Tag = "18";
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(248, 535);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(64, 26);
            this.textBox17.TabIndex = 31;
            this.textBox17.Tag = "17";
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(248, 502);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(64, 26);
            this.textBox16.TabIndex = 31;
            this.textBox16.Tag = "16";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(248, 469);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(64, 26);
            this.textBox15.TabIndex = 31;
            this.textBox15.Tag = "15";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(248, 436);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(64, 26);
            this.textBox14.TabIndex = 31;
            this.textBox14.Tag = "14";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(248, 403);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(64, 26);
            this.textBox13.TabIndex = 31;
            this.textBox13.Tag = "13";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(248, 370);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(64, 26);
            this.textBox12.TabIndex = 31;
            this.textBox12.Tag = "12";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(248, 337);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(64, 26);
            this.textBox11.TabIndex = 31;
            this.textBox11.Tag = "11";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(248, 304);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(64, 26);
            this.textBox10.TabIndex = 31;
            this.textBox10.Tag = "10";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(248, 271);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(64, 26);
            this.textBox9.TabIndex = 31;
            this.textBox9.Tag = "9";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(248, 238);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(64, 26);
            this.textBox8.TabIndex = 31;
            this.textBox8.Tag = "8";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(248, 205);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(64, 26);
            this.textBox7.TabIndex = 31;
            this.textBox7.Tag = "7";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(248, 172);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(64, 26);
            this.textBox6.TabIndex = 31;
            this.textBox6.Tag = "6";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(248, 139);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(64, 26);
            this.textBox5.TabIndex = 31;
            this.textBox5.Tag = "5";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(248, 106);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(64, 26);
            this.textBox4.TabIndex = 31;
            this.textBox4.Tag = "4";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(248, 73);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(64, 26);
            this.textBox3.TabIndex = 31;
            this.textBox3.Tag = "3";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(248, 40);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(64, 26);
            this.textBox2.TabIndex = 31;
            this.textBox2.Tag = "2";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(248, 7);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(64, 26);
            this.textBox1.TabIndex = 30;
            this.textBox1.Tag = "1";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(7, 4);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(144, 16);
            this.label22.TabIndex = 2;
            this.label22.Text = "Auvergne-Rhône-Alpes";
            // 
            // label_ARA
            // 
            this.label_ARA.AutoSize = true;
            this.label_ARA.Location = new System.Drawing.Point(198, 4);
            this.label_ARA.Name = "label_ARA";
            this.label_ARA.Size = new System.Drawing.Size(38, 30);
            this.label_ARA.TabIndex = 0;
            this.label_ARA.Text = "label_ARA";
            // 
            // label_BFC
            // 
            this.label_BFC.AutoSize = true;
            this.label_BFC.Location = new System.Drawing.Point(198, 37);
            this.label_BFC.Name = "label_BFC";
            this.label_BFC.Size = new System.Drawing.Size(36, 30);
            this.label_BFC.TabIndex = 1;
            this.label_BFC.Text = "label_BFC";
            // 
            // label_Bre
            // 
            this.label_Bre.AutoSize = true;
            this.label_Bre.Location = new System.Drawing.Point(198, 70);
            this.label_Bre.Name = "label_Bre";
            this.label_Bre.Size = new System.Drawing.Size(36, 30);
            this.label_Bre.TabIndex = 2;
            this.label_Bre.Text = "label_Bre";
            // 
            // label_CVL
            // 
            this.label_CVL.AutoSize = true;
            this.label_CVL.Location = new System.Drawing.Point(198, 103);
            this.label_CVL.Name = "label_CVL";
            this.label_CVL.Size = new System.Drawing.Size(36, 30);
            this.label_CVL.TabIndex = 3;
            this.label_CVL.Text = "label_CVL";
            // 
            // label_Cor
            // 
            this.label_Cor.AutoSize = true;
            this.label_Cor.Location = new System.Drawing.Point(198, 136);
            this.label_Cor.Name = "label_Cor";
            this.label_Cor.Size = new System.Drawing.Size(36, 30);
            this.label_Cor.TabIndex = 4;
            this.label_Cor.Text = "label_Cor";
            // 
            // label_GrE
            // 
            this.label_GrE.AutoSize = true;
            this.label_GrE.Location = new System.Drawing.Point(198, 169);
            this.label_GrE.Name = "label_GrE";
            this.label_GrE.Size = new System.Drawing.Size(36, 30);
            this.label_GrE.TabIndex = 5;
            this.label_GrE.Text = "label_GrE";
            // 
            // label_HdF
            // 
            this.label_HdF.AutoSize = true;
            this.label_HdF.Location = new System.Drawing.Point(198, 202);
            this.label_HdF.Name = "label_HdF";
            this.label_HdF.Size = new System.Drawing.Size(37, 30);
            this.label_HdF.TabIndex = 6;
            this.label_HdF.Text = "label_HdF";
            // 
            // label_IdF
            // 
            this.label_IdF.AutoSize = true;
            this.label_IdF.Location = new System.Drawing.Point(198, 235);
            this.label_IdF.Name = "label_IdF";
            this.label_IdF.Size = new System.Drawing.Size(36, 30);
            this.label_IdF.TabIndex = 7;
            this.label_IdF.Text = "label_IdF";
            // 
            // label_Nor
            // 
            this.label_Nor.AutoSize = true;
            this.label_Nor.Location = new System.Drawing.Point(198, 268);
            this.label_Nor.Name = "label_Nor";
            this.label_Nor.Size = new System.Drawing.Size(36, 30);
            this.label_Nor.TabIndex = 8;
            this.label_Nor.Text = "label_Nor";
            // 
            // label_NAq
            // 
            this.label_NAq.AutoSize = true;
            this.label_NAq.Location = new System.Drawing.Point(198, 301);
            this.label_NAq.Name = "label_NAq";
            this.label_NAq.Size = new System.Drawing.Size(39, 30);
            this.label_NAq.TabIndex = 9;
            this.label_NAq.Text = "label_NAq";
            // 
            // label_Occ
            // 
            this.label_Occ.AutoSize = true;
            this.label_Occ.Location = new System.Drawing.Point(198, 334);
            this.label_Occ.Name = "label_Occ";
            this.label_Occ.Size = new System.Drawing.Size(36, 30);
            this.label_Occ.TabIndex = 10;
            this.label_Occ.Text = "label_Occ";
            // 
            // label_PdL
            // 
            this.label_PdL.AutoSize = true;
            this.label_PdL.Location = new System.Drawing.Point(198, 367);
            this.label_PdL.Name = "label_PdL";
            this.label_PdL.Size = new System.Drawing.Size(36, 30);
            this.label_PdL.TabIndex = 11;
            this.label_PdL.Text = "label_PdL";
            // 
            // label_PACA
            // 
            this.label_PACA.AutoSize = true;
            this.label_PACA.Location = new System.Drawing.Point(198, 400);
            this.label_PACA.Name = "label_PACA";
            this.label_PACA.Size = new System.Drawing.Size(37, 30);
            this.label_PACA.TabIndex = 12;
            this.label_PACA.Text = "label_PACA";
            // 
            // label_Gua
            // 
            this.label_Gua.AutoSize = true;
            this.label_Gua.Location = new System.Drawing.Point(198, 433);
            this.label_Gua.Name = "label_Gua";
            this.label_Gua.Size = new System.Drawing.Size(37, 30);
            this.label_Gua.TabIndex = 13;
            this.label_Gua.Text = "label_Gua";
            // 
            // label_Guy
            // 
            this.label_Guy.AutoSize = true;
            this.label_Guy.Location = new System.Drawing.Point(198, 466);
            this.label_Guy.Name = "label_Guy";
            this.label_Guy.Size = new System.Drawing.Size(36, 30);
            this.label_Guy.TabIndex = 14;
            this.label_Guy.Text = "label_Guy";
            // 
            // label_Mar
            // 
            this.label_Mar.AutoSize = true;
            this.label_Mar.Location = new System.Drawing.Point(198, 499);
            this.label_Mar.Name = "label_Mar";
            this.label_Mar.Size = new System.Drawing.Size(36, 30);
            this.label_Mar.TabIndex = 15;
            this.label_Mar.Text = "label_Mar";
            // 
            // label_Reu
            // 
            this.label_Reu.AutoSize = true;
            this.label_Reu.Location = new System.Drawing.Point(198, 532);
            this.label_Reu.Name = "label_Reu";
            this.label_Reu.Size = new System.Drawing.Size(36, 30);
            this.label_Reu.TabIndex = 16;
            this.label_Reu.Text = "label_Reu";
            // 
            // label_May
            // 
            this.label_May.AutoSize = true;
            this.label_May.Location = new System.Drawing.Point(198, 565);
            this.label_May.Name = "label_May";
            this.label_May.Size = new System.Drawing.Size(37, 30);
            this.label_May.TabIndex = 17;
            this.label_May.Text = "label_May";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 37);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(165, 16);
            this.label23.TabIndex = 18;
            this.label23.Text = "Bourgogne-Franche-Comté";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(7, 70);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(59, 16);
            this.label24.TabIndex = 19;
            this.label24.Text = "Bretagne";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 103);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(122, 16);
            this.label25.TabIndex = 20;
            this.label25.Text = "Centre-Val de Loire";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(7, 136);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(41, 16);
            this.label26.TabIndex = 21;
            this.label26.Text = "Corse";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(7, 169);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(65, 16);
            this.label27.TabIndex = 22;
            this.label27.Text = "Grand Est";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(7, 202);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(108, 16);
            this.label28.TabIndex = 23;
            this.label28.Text = "Hauts-de-France";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(7, 235);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(88, 16);
            this.label29.TabIndex = 24;
            this.label29.Text = "Île-de-France";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(7, 268);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(72, 16);
            this.label30.TabIndex = 25;
            this.label30.Text = "Normandie";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(7, 301);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(118, 16);
            this.label31.TabIndex = 26;
            this.label31.Text = "Nouvelle-Aquitaine";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(7, 334);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(60, 16);
            this.label32.TabIndex = 27;
            this.label32.Text = "Occitanie";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(7, 367);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(99, 16);
            this.label33.TabIndex = 28;
            this.label33.Text = "Pays de la Loire";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(7, 400);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(174, 16);
            this.label34.TabIndex = 29;
            this.label34.Text = "Provence-Alpes-Côte d\'Azur";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(7, 433);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(82, 16);
            this.label35.TabIndex = 30;
            this.label35.Text = "Guadeloupe ";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(7, 466);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(115, 16);
            this.label36.TabIndex = 31;
            this.label36.Text = "Guyane (française)";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(7, 499);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(73, 16);
            this.label37.TabIndex = 32;
            this.label37.Text = "Martinique ";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(7, 532);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(71, 16);
            this.label38.TabIndex = 33;
            this.label38.Text = "La Réunion";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(7, 565);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(53, 16);
            this.label39.TabIndex = 34;
            this.label39.Text = "Mayotte";
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(321, 370);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 31;
            this.button11.Text = "Modifier";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel4.Controls.Add(this.btn_quiter);
            this.panel4.Controls.Add(this.btn_deconnexion);
            this.panel4.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel4.Location = new System.Drawing.Point(443, 627);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(613, 64);
            this.panel4.TabIndex = 29;
            // 
            // btn_quiter
            // 
            this.btn_quiter.BackColor = System.Drawing.SystemColors.Control;
            this.btn_quiter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_quiter.Font = new System.Drawing.Font("Liberation Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_quiter.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_quiter.Location = new System.Drawing.Point(510, 19);
            this.btn_quiter.Name = "btn_quiter";
            this.btn_quiter.Size = new System.Drawing.Size(100, 30);
            this.btn_quiter.TabIndex = 29;
            this.btn_quiter.Text = "Quitter";
            this.btn_quiter.UseVisualStyleBackColor = false;
            this.btn_quiter.Click += new System.EventHandler(this.btn_quiter_Click);
            // 
            // btn_deconnexion
            // 
            this.btn_deconnexion.BackColor = System.Drawing.SystemColors.Control;
            this.btn_deconnexion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_deconnexion.Font = new System.Drawing.Font("Liberation Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_deconnexion.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_deconnexion.Location = new System.Drawing.Point(370, 19);
            this.btn_deconnexion.Name = "btn_deconnexion";
            this.btn_deconnexion.Size = new System.Drawing.Size(123, 30);
            this.btn_deconnexion.TabIndex = 27;
            this.btn_deconnexion.Text = "Déconnexion";
            this.btn_deconnexion.UseVisualStyleBackColor = false;
            this.btn_deconnexion.Click += new System.EventHandler(this.btn_deconnexion_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "Nom de la région";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(220, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 31;
            this.label8.Text = "Places";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(269, 79);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 13);
            this.label17.TabIndex = 32;
            this.label17.Text = "Nouvelles places";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.Highlight;
            this.dataGridView1.Location = new System.Drawing.Point(443, 396);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(613, 214);
            this.dataGridView1.TabIndex = 33;
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(492, 358);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(109, 21);
            this.comboBox2.TabIndex = 34;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(443, 359);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(48, 18);
            this.label18.TabIndex = 35;
            this.label18.Text = "Filtre :";
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(680, 358);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(182, 21);
            this.comboBox3.TabIndex = 36;
            this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // lbl_filtre
            // 
            this.lbl_filtre.AutoSize = true;
            this.lbl_filtre.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_filtre.Location = new System.Drawing.Point(624, 359);
            this.lbl_filtre.Name = "lbl_filtre";
            this.lbl_filtre.Size = new System.Drawing.Size(48, 18);
            this.lbl_filtre.TabIndex = 37;
            this.lbl_filtre.Text = "Filtre :";
            this.lbl_filtre.Visible = false;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(878, 359);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(178, 20);
            this.dateTimePicker1.TabIndex = 39;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // AffectationVoeux
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1068, 694);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.lbl_filtre);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel1);
            this.Name = "AffectationVoeux";
            this.Text = "date";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label_ARA;
        private System.Windows.Forms.Label label_BFC;
        private System.Windows.Forms.Label label_Bre;
        private System.Windows.Forms.Label label_CVL;
        private System.Windows.Forms.Label label_Cor;
        private System.Windows.Forms.Label label_GrE;
        private System.Windows.Forms.Label label_HdF;
        private System.Windows.Forms.Label label_IdF;
        private System.Windows.Forms.Label label_Nor;
        private System.Windows.Forms.Label label_NAq;
        private System.Windows.Forms.Label label_Occ;
        private System.Windows.Forms.Label label_PdL;
        private System.Windows.Forms.Label label_PACA;
        private System.Windows.Forms.Label label_Gua;
        private System.Windows.Forms.Label label_Guy;
        private System.Windows.Forms.Label label_Mar;
        private System.Windows.Forms.Label label_Reu;
        private System.Windows.Forms.Label label_May;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button btn_accepter;
        private System.Windows.Forms.Button btn_refuser;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btn_quiter;
        private System.Windows.Forms.Button btn_deconnexion;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label_v_point;
        private System.Windows.Forms.Label label_v_nom;
        private System.Windows.Forms.Label label_v_prenom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_refuser_3;
        private System.Windows.Forms.Button btn_refuser_2;
        private System.Windows.Forms.Button btn_accepter_3;
        private System.Windows.Forms.Button btn_accepter_2;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label lbl_filtre;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}