﻿namespace PPE_4_CS
{
    partial class FormConnexion
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_connexion = new System.Windows.Forms.Button();
            this.textBox_c_login = new System.Windows.Forms.TextBox();
            this.textBox_c_mdp = new System.Windows.Forms.TextBox();
            this.panel_connexion = new System.Windows.Forms.Panel();
            this.label_message_c = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_inscription = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel_inscription = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.btn_annuler = new System.Windows.Forms.Button();
            this.textBox_i_annee = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox_i_statut = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_i_nom = new System.Windows.Forms.TextBox();
            this.textBox_i_prenom = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label_message_i = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_inscription_i = new System.Windows.Forms.Button();
            this.textBox_i_mdp = new System.Windows.Forms.TextBox();
            this.textBox_i_login = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panel_connexion.SuspendLayout();
            this.panel_inscription.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_connexion
            // 
            this.btn_connexion.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btn_connexion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_connexion.FlatAppearance.BorderColor = System.Drawing.Color.MidnightBlue;
            this.btn_connexion.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_connexion.Location = new System.Drawing.Point(45, 212);
            this.btn_connexion.Name = "btn_connexion";
            this.btn_connexion.Size = new System.Drawing.Size(109, 34);
            this.btn_connexion.TabIndex = 0;
            this.btn_connexion.Text = "Connexion";
            this.btn_connexion.UseVisualStyleBackColor = false;
            this.btn_connexion.Click += new System.EventHandler(this.btn_connexion_Click);
            // 
            // textBox_c_login
            // 
            this.textBox_c_login.Location = new System.Drawing.Point(26, 112);
            this.textBox_c_login.Name = "textBox_c_login";
            this.textBox_c_login.Size = new System.Drawing.Size(149, 20);
            this.textBox_c_login.TabIndex = 1;
            this.textBox_c_login.Text = "test_login";
            // 
            // textBox_c_mdp
            // 
            this.textBox_c_mdp.Location = new System.Drawing.Point(26, 169);
            this.textBox_c_mdp.Name = "textBox_c_mdp";
            this.textBox_c_mdp.PasswordChar = '*';
            this.textBox_c_mdp.Size = new System.Drawing.Size(149, 20);
            this.textBox_c_mdp.TabIndex = 2;
            this.textBox_c_mdp.Text = "test_mot_de_passe";
            // 
            // panel_connexion
            // 
            this.panel_connexion.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel_connexion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_connexion.Controls.Add(this.label_message_c);
            this.panel_connexion.Controls.Add(this.label6);
            this.panel_connexion.Controls.Add(this.btn_inscription);
            this.panel_connexion.Controls.Add(this.label2);
            this.panel_connexion.Controls.Add(this.label1);
            this.panel_connexion.Controls.Add(this.textBox_c_login);
            this.panel_connexion.Controls.Add(this.btn_connexion);
            this.panel_connexion.Controls.Add(this.textBox_c_mdp);
            this.panel_connexion.Location = new System.Drawing.Point(342, 138);
            this.panel_connexion.Name = "panel_connexion";
            this.panel_connexion.Size = new System.Drawing.Size(200, 300);
            this.panel_connexion.TabIndex = 3;
            // 
            // label_message_c
            // 
            this.label_message_c.AutoSize = true;
            this.label_message_c.Location = new System.Drawing.Point(23, 56);
            this.label_message_c.Name = "label_message_c";
            this.label_message_c.Size = new System.Drawing.Size(49, 13);
            this.label_message_c.TabIndex = 14;
            this.label_message_c.Text = "message";
            this.label_message_c.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label6.Location = new System.Drawing.Point(47, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 25);
            this.label6.TabIndex = 13;
            this.label6.Text = "Connexion";
            // 
            // btn_inscription
            // 
            this.btn_inscription.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_inscription.Location = new System.Drawing.Point(45, 252);
            this.btn_inscription.Name = "btn_inscription";
            this.btn_inscription.Size = new System.Drawing.Size(109, 23);
            this.btn_inscription.TabIndex = 5;
            this.btn_inscription.Text = "Inscription";
            this.btn_inscription.UseVisualStyleBackColor = true;
            this.btn_inscription.Click += new System.EventHandler(this.btn_inscription_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(61, 153);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Mot de passe";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(70, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Identifiant";
            // 
            // panel_inscription
            // 
            this.panel_inscription.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel_inscription.Controls.Add(this.label13);
            this.panel_inscription.Controls.Add(this.label12);
            this.panel_inscription.Controls.Add(this.btn_annuler);
            this.panel_inscription.Controls.Add(this.textBox_i_annee);
            this.panel_inscription.Controls.Add(this.label10);
            this.panel_inscription.Controls.Add(this.label9);
            this.panel_inscription.Controls.Add(this.textBox_i_statut);
            this.panel_inscription.Controls.Add(this.label8);
            this.panel_inscription.Controls.Add(this.label7);
            this.panel_inscription.Controls.Add(this.textBox_i_nom);
            this.panel_inscription.Controls.Add(this.textBox_i_prenom);
            this.panel_inscription.Controls.Add(this.label5);
            this.panel_inscription.Controls.Add(this.label_message_i);
            this.panel_inscription.Controls.Add(this.label3);
            this.panel_inscription.Controls.Add(this.label4);
            this.panel_inscription.Controls.Add(this.btn_inscription_i);
            this.panel_inscription.Controls.Add(this.textBox_i_mdp);
            this.panel_inscription.Controls.Add(this.textBox_i_login);
            this.panel_inscription.Location = new System.Drawing.Point(567, 94);
            this.panel_inscription.Name = "panel_inscription";
            this.panel_inscription.Size = new System.Drawing.Size(274, 429);
            this.panel_inscription.TabIndex = 4;
            this.panel_inscription.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(87, 247);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(144, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "1: Visiteur | 2 : RH | 3 : Admin";
            // 
            // btn_annuler
            // 
            this.btn_annuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_annuler.Location = new System.Drawing.Point(146, 359);
            this.btn_annuler.Name = "btn_annuler";
            this.btn_annuler.Size = new System.Drawing.Size(109, 23);
            this.btn_annuler.TabIndex = 20;
            this.btn_annuler.Text = "Annuler";
            this.btn_annuler.UseVisualStyleBackColor = true;
            this.btn_annuler.Click += new System.EventHandler(this.btn_annuler_Click);
            // 
            // textBox_i_annee
            // 
            this.textBox_i_annee.Location = new System.Drawing.Point(90, 313);
            this.textBox_i_annee.Name = "textBox_i_annee";
            this.textBox_i_annee.Size = new System.Drawing.Size(165, 20);
            this.textBox_i_annee.TabIndex = 19;
            this.textBox_i_annee.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_i_annee_KeyDown);
            this.textBox_i_annee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_i_annee_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 316);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "année d\'entrée";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 270);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "statut";
            // 
            // textBox_i_statut
            // 
            this.textBox_i_statut.Location = new System.Drawing.Point(90, 263);
            this.textBox_i_statut.Name = "textBox_i_statut";
            this.textBox_i_statut.Size = new System.Drawing.Size(165, 20);
            this.textBox_i_statut.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 219);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "nom";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "prenom";
            // 
            // textBox_i_nom
            // 
            this.textBox_i_nom.Location = new System.Drawing.Point(90, 216);
            this.textBox_i_nom.Name = "textBox_i_nom";
            this.textBox_i_nom.Size = new System.Drawing.Size(165, 20);
            this.textBox_i_nom.TabIndex = 13;
            // 
            // textBox_i_prenom
            // 
            this.textBox_i_prenom.Location = new System.Drawing.Point(90, 171);
            this.textBox_i_prenom.Name = "textBox_i_prenom";
            this.textBox_i_prenom.Size = new System.Drawing.Size(165, 20);
            this.textBox_i_prenom.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(85, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 25);
            this.label5.TabIndex = 11;
            this.label5.Text = "Inscription";
            // 
            // label_message_i
            // 
            this.label_message_i.AutoSize = true;
            this.label_message_i.Location = new System.Drawing.Point(87, 44);
            this.label_message_i.Name = "label_message_i";
            this.label_message_i.Size = new System.Drawing.Size(49, 13);
            this.label_message_i.TabIndex = 10;
            this.label_message_i.Text = "message";
            this.label_message_i.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "mot de passe";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "login";
            // 
            // btn_inscription_i
            // 
            this.btn_inscription_i.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_inscription_i.Location = new System.Drawing.Point(14, 359);
            this.btn_inscription_i.Name = "btn_inscription_i";
            this.btn_inscription_i.Size = new System.Drawing.Size(121, 23);
            this.btn_inscription_i.TabIndex = 6;
            this.btn_inscription_i.Text = "Inscription";
            this.btn_inscription_i.UseVisualStyleBackColor = true;
            this.btn_inscription_i.Click += new System.EventHandler(this.btn_inscription_i_Click);
            // 
            // textBox_i_mdp
            // 
            this.textBox_i_mdp.Location = new System.Drawing.Point(90, 126);
            this.textBox_i_mdp.Name = "textBox_i_mdp";
            this.textBox_i_mdp.PasswordChar = '*';
            this.textBox_i_mdp.Size = new System.Drawing.Size(165, 20);
            this.textBox_i_mdp.TabIndex = 2;
            // 
            // textBox_i_login
            // 
            this.textBox_i_login.Location = new System.Drawing.Point(90, 83);
            this.textBox_i_login.Name = "textBox_i_login";
            this.textBox_i_login.Size = new System.Drawing.Size(165, 20);
            this.textBox_i_login.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Controls.Add(this.label11);
            this.panel1.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(860, 55);
            this.panel1.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label11.Location = new System.Drawing.Point(252, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(410, 46);
            this.label11.TabIndex = 0;
            this.label11.Text = "Galaxy Swiss Bourdin";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel2.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(12, 545);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(860, 55);
            this.panel2.TabIndex = 6;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label13.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label13.Location = new System.Drawing.Point(11, 394);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "label13";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(884, 612);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel_inscription);
            this.Controls.Add(this.panel_connexion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel_connexion.ResumeLayout(false);
            this.panel_connexion.PerformLayout();
            this.panel_inscription.ResumeLayout(false);
            this.panel_inscription.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_connexion;
        private System.Windows.Forms.TextBox textBox_c_login;
        private System.Windows.Forms.TextBox textBox_c_mdp;
        private System.Windows.Forms.Panel panel_connexion;
        private System.Windows.Forms.Button btn_inscription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel_inscription;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox_i_statut;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_i_nom;
        private System.Windows.Forms.TextBox textBox_i_prenom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label_message_i;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_inscription_i;
        private System.Windows.Forms.TextBox textBox_i_mdp;
        private System.Windows.Forms.TextBox textBox_i_login;
        private System.Windows.Forms.Label label_message_c;
        private System.Windows.Forms.TextBox textBox_i_annee;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_annuler;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
    }
}

