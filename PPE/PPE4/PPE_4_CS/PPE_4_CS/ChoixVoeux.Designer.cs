﻿namespace PPE_4_CS
{
    partial class ChoixVoeux
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.comboBox9 = new System.Windows.Forms.ComboBox();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.comboBox11 = new System.Windows.Forms.ComboBox();
            this.comboBox12 = new System.Windows.Forms.ComboBox();
            this.comboBox13 = new System.Windows.Forms.ComboBox();
            this.comboBox14 = new System.Windows.Forms.ComboBox();
            this.comboBox15 = new System.Windows.Forms.ComboBox();
            this.comboBox16 = new System.Windows.Forms.ComboBox();
            this.comboBox17 = new System.Windows.Forms.ComboBox();
            this.comboBox18 = new System.Windows.Forms.ComboBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label_ARA = new System.Windows.Forms.Label();
            this.label_BFC = new System.Windows.Forms.Label();
            this.label_Bre = new System.Windows.Forms.Label();
            this.label_CVL = new System.Windows.Forms.Label();
            this.label_Cor = new System.Windows.Forms.Label();
            this.label_GrE = new System.Windows.Forms.Label();
            this.label_HdF = new System.Windows.Forms.Label();
            this.label_IdF = new System.Windows.Forms.Label();
            this.label_Nor = new System.Windows.Forms.Label();
            this.label_NAq = new System.Windows.Forms.Label();
            this.label_Occ = new System.Windows.Forms.Label();
            this.label_PdL = new System.Windows.Forms.Label();
            this.label_PACA = new System.Windows.Forms.Label();
            this.label_Gua = new System.Windows.Forms.Label();
            this.label_Guy = new System.Windows.Forms.Label();
            this.label_Mar = new System.Windows.Forms.Label();
            this.label_Reu = new System.Windows.Forms.Label();
            this.label_May = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label_reg_1 = new System.Windows.Forms.Label();
            this.label_reg_2 = new System.Windows.Forms.Label();
            this.label_reg_3 = new System.Windows.Forms.Label();
            this.label_rang_1 = new System.Windows.Forms.Label();
            this.label_rang_2 = new System.Windows.Forms.Label();
            this.label_rang_3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label_nom_prenom = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.btn_valider = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.btn_deconnexion = new System.Windows.Forms.Button();
            this.button_modifier = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_annuler = new System.Windows.Forms.Button();
            this.btn_annul_dem_voeux = new System.Windows.Forms.Button();
            this.btn_annuler_2 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(12, 73);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(437, 644);
            this.panel1.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(283, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Ordre des voeux";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(230, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Places";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetPartial;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 79.941F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.059F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutPanel1.Controls.Add(this.comboBox1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.comboBox2, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.comboBox3, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.comboBox4, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.comboBox5, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.comboBox6, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.comboBox7, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.comboBox8, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.comboBox9, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.comboBox10, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.comboBox11, 2, 10);
            this.tableLayoutPanel1.Controls.Add(this.comboBox12, 2, 11);
            this.tableLayoutPanel1.Controls.Add(this.comboBox13, 2, 12);
            this.tableLayoutPanel1.Controls.Add(this.comboBox14, 2, 13);
            this.tableLayoutPanel1.Controls.Add(this.comboBox15, 2, 14);
            this.tableLayoutPanel1.Controls.Add(this.comboBox16, 2, 15);
            this.tableLayoutPanel1.Controls.Add(this.comboBox17, 2, 16);
            this.tableLayoutPanel1.Controls.Add(this.comboBox18, 2, 17);
            this.tableLayoutPanel1.Controls.Add(this.checkBox2, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.checkBox1, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBox3, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.checkBox4, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.checkBox5, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.checkBox6, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.checkBox7, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.checkBox8, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.checkBox9, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.checkBox10, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.checkBox11, 3, 10);
            this.tableLayoutPanel1.Controls.Add(this.checkBox12, 3, 11);
            this.tableLayoutPanel1.Controls.Add(this.checkBox13, 3, 12);
            this.tableLayoutPanel1.Controls.Add(this.checkBox14, 3, 13);
            this.tableLayoutPanel1.Controls.Add(this.checkBox15, 3, 14);
            this.tableLayoutPanel1.Controls.Add(this.checkBox16, 3, 15);
            this.tableLayoutPanel1.Controls.Add(this.checkBox17, 3, 16);
            this.tableLayoutPanel1.Controls.Add(this.checkBox18, 3, 17);
            this.tableLayoutPanel1.Controls.Add(this.label22, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label_ARA, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label_BFC, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label_Bre, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label_CVL, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label_Cor, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label_GrE, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label_HdF, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label_IdF, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label_Nor, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.label_NAq, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.label_Occ, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.label_PdL, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.label_PACA, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.label_Gua, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.label_Guy, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.label_Mar, 1, 15);
            this.tableLayoutPanel1.Controls.Add(this.label_Reu, 1, 16);
            this.tableLayoutPanel1.Controls.Add(this.label_May, 1, 17);
            this.tableLayoutPanel1.Controls.Add(this.label23, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label24, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label25, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label26, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label27, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label28, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label29, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label30, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label31, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label32, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.label33, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.label34, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.label35, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.label36, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.label37, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this.label38, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this.label39, 0, 17);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 37);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel1.RowCount = 18;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(425, 596);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(268, 7);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(59, 24);
            this.comboBox1.TabIndex = 35;
            this.comboBox1.Tag = "1";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.ItemHeight = 16;
            this.comboBox2.Location = new System.Drawing.Point(268, 40);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(59, 24);
            this.comboBox2.TabIndex = 36;
            this.comboBox2.Tag = "2";
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(268, 73);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(59, 24);
            this.comboBox3.TabIndex = 47;
            this.comboBox3.Tag = "3";
            this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(268, 106);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(59, 24);
            this.comboBox4.TabIndex = 36;
            this.comboBox4.Tag = "4";
            this.comboBox4.SelectedIndexChanged += new System.EventHandler(this.comboBox4_SelectedIndexChanged);
            // 
            // comboBox5
            // 
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(268, 139);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(59, 24);
            this.comboBox5.TabIndex = 36;
            this.comboBox5.Tag = "5";
            this.comboBox5.SelectedIndexChanged += new System.EventHandler(this.comboBox5_SelectedIndexChanged);
            // 
            // comboBox6
            // 
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(268, 172);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(59, 24);
            this.comboBox6.TabIndex = 36;
            this.comboBox6.Tag = "6";
            this.comboBox6.SelectedIndexChanged += new System.EventHandler(this.comboBox6_SelectedIndexChanged);
            // 
            // comboBox7
            // 
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Location = new System.Drawing.Point(268, 205);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(59, 24);
            this.comboBox7.TabIndex = 36;
            this.comboBox7.Tag = "7";
            this.comboBox7.SelectedIndexChanged += new System.EventHandler(this.comboBox7_SelectedIndexChanged);
            // 
            // comboBox8
            // 
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Location = new System.Drawing.Point(268, 238);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(59, 24);
            this.comboBox8.TabIndex = 37;
            this.comboBox8.Tag = "8";
            this.comboBox8.SelectedIndexChanged += new System.EventHandler(this.comboBox8_SelectedIndexChanged);
            // 
            // comboBox9
            // 
            this.comboBox9.FormattingEnabled = true;
            this.comboBox9.Location = new System.Drawing.Point(268, 271);
            this.comboBox9.Name = "comboBox9";
            this.comboBox9.Size = new System.Drawing.Size(59, 24);
            this.comboBox9.TabIndex = 36;
            this.comboBox9.Tag = "9";
            this.comboBox9.SelectedIndexChanged += new System.EventHandler(this.comboBox9_SelectedIndexChanged);
            // 
            // comboBox10
            // 
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Location = new System.Drawing.Point(268, 304);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(59, 24);
            this.comboBox10.TabIndex = 38;
            this.comboBox10.Tag = "10";
            this.comboBox10.SelectedIndexChanged += new System.EventHandler(this.comboBox10_SelectedIndexChanged);
            // 
            // comboBox11
            // 
            this.comboBox11.FormattingEnabled = true;
            this.comboBox11.Location = new System.Drawing.Point(268, 337);
            this.comboBox11.Name = "comboBox11";
            this.comboBox11.Size = new System.Drawing.Size(59, 24);
            this.comboBox11.TabIndex = 39;
            this.comboBox11.Tag = "11";
            this.comboBox11.SelectedIndexChanged += new System.EventHandler(this.comboBox11_SelectedIndexChanged);
            // 
            // comboBox12
            // 
            this.comboBox12.FormattingEnabled = true;
            this.comboBox12.Location = new System.Drawing.Point(268, 370);
            this.comboBox12.Name = "comboBox12";
            this.comboBox12.Size = new System.Drawing.Size(59, 24);
            this.comboBox12.TabIndex = 40;
            this.comboBox12.Tag = "12";
            this.comboBox12.SelectedIndexChanged += new System.EventHandler(this.comboBox12_SelectedIndexChanged);
            // 
            // comboBox13
            // 
            this.comboBox13.FormattingEnabled = true;
            this.comboBox13.Location = new System.Drawing.Point(268, 403);
            this.comboBox13.Name = "comboBox13";
            this.comboBox13.Size = new System.Drawing.Size(59, 24);
            this.comboBox13.TabIndex = 41;
            this.comboBox13.Tag = "13";
            this.comboBox13.SelectedIndexChanged += new System.EventHandler(this.comboBox13_SelectedIndexChanged);
            // 
            // comboBox14
            // 
            this.comboBox14.FormattingEnabled = true;
            this.comboBox14.Location = new System.Drawing.Point(268, 436);
            this.comboBox14.Name = "comboBox14";
            this.comboBox14.Size = new System.Drawing.Size(59, 24);
            this.comboBox14.TabIndex = 42;
            this.comboBox14.Tag = "14";
            this.comboBox14.SelectedIndexChanged += new System.EventHandler(this.comboBox14_SelectedIndexChanged);
            // 
            // comboBox15
            // 
            this.comboBox15.FormattingEnabled = true;
            this.comboBox15.Location = new System.Drawing.Point(268, 469);
            this.comboBox15.Name = "comboBox15";
            this.comboBox15.Size = new System.Drawing.Size(59, 24);
            this.comboBox15.TabIndex = 43;
            this.comboBox15.Tag = "15";
            this.comboBox15.SelectedIndexChanged += new System.EventHandler(this.comboBox15_SelectedIndexChanged);
            // 
            // comboBox16
            // 
            this.comboBox16.FormattingEnabled = true;
            this.comboBox16.Location = new System.Drawing.Point(268, 502);
            this.comboBox16.Name = "comboBox16";
            this.comboBox16.Size = new System.Drawing.Size(59, 24);
            this.comboBox16.TabIndex = 44;
            this.comboBox16.Tag = "16";
            this.comboBox16.SelectedIndexChanged += new System.EventHandler(this.comboBox16_SelectedIndexChanged);
            // 
            // comboBox17
            // 
            this.comboBox17.FormattingEnabled = true;
            this.comboBox17.Location = new System.Drawing.Point(268, 535);
            this.comboBox17.Name = "comboBox17";
            this.comboBox17.Size = new System.Drawing.Size(59, 24);
            this.comboBox17.TabIndex = 45;
            this.comboBox17.Tag = "17";
            this.comboBox17.SelectedIndexChanged += new System.EventHandler(this.comboBox17_SelectedIndexChanged);
            // 
            // comboBox18
            // 
            this.comboBox18.FormattingEnabled = true;
            this.comboBox18.Location = new System.Drawing.Point(268, 568);
            this.comboBox18.Name = "comboBox18";
            this.comboBox18.Size = new System.Drawing.Size(59, 24);
            this.comboBox18.TabIndex = 46;
            this.comboBox18.Tag = "18";
            this.comboBox18.SelectedIndexChanged += new System.EventHandler(this.comboBox18_SelectedIndexChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(372, 45);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 7;
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(372, 12);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 7;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(372, 78);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 8;
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(372, 111);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(15, 14);
            this.checkBox4.TabIndex = 9;
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(372, 144);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(15, 14);
            this.checkBox5.TabIndex = 10;
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // checkBox6
            // 
            this.checkBox6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(372, 177);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(15, 14);
            this.checkBox6.TabIndex = 11;
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(372, 210);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(15, 14);
            this.checkBox7.TabIndex = 12;
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // checkBox8
            // 
            this.checkBox8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(372, 243);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(15, 14);
            this.checkBox8.TabIndex = 13;
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.checkBox8_CheckedChanged);
            // 
            // checkBox9
            // 
            this.checkBox9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(372, 276);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(15, 14);
            this.checkBox9.TabIndex = 14;
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.checkBox9_CheckedChanged);
            // 
            // checkBox10
            // 
            this.checkBox10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(372, 309);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(15, 14);
            this.checkBox10.TabIndex = 15;
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.checkBox10_CheckedChanged);
            // 
            // checkBox11
            // 
            this.checkBox11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(372, 342);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(15, 14);
            this.checkBox11.TabIndex = 16;
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.CheckedChanged += new System.EventHandler(this.checkBox11_CheckedChanged);
            // 
            // checkBox12
            // 
            this.checkBox12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(372, 375);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(15, 14);
            this.checkBox12.TabIndex = 17;
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.checkBox12_CheckedChanged);
            // 
            // checkBox13
            // 
            this.checkBox13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(372, 408);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(15, 14);
            this.checkBox13.TabIndex = 19;
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.CheckedChanged += new System.EventHandler(this.checkBox13_CheckedChanged);
            // 
            // checkBox14
            // 
            this.checkBox14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(372, 441);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(15, 14);
            this.checkBox14.TabIndex = 19;
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.CheckedChanged += new System.EventHandler(this.checkBox14_CheckedChanged);
            // 
            // checkBox15
            // 
            this.checkBox15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(372, 474);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(15, 14);
            this.checkBox15.TabIndex = 19;
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.CheckedChanged += new System.EventHandler(this.checkBox15_CheckedChanged);
            // 
            // checkBox16
            // 
            this.checkBox16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(372, 507);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(15, 14);
            this.checkBox16.TabIndex = 19;
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.CheckedChanged += new System.EventHandler(this.checkBox16_CheckedChanged);
            // 
            // checkBox17
            // 
            this.checkBox17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox17.AutoSize = true;
            this.checkBox17.Location = new System.Drawing.Point(372, 540);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(15, 14);
            this.checkBox17.TabIndex = 19;
            this.checkBox17.UseVisualStyleBackColor = true;
            this.checkBox17.CheckedChanged += new System.EventHandler(this.checkBox17_CheckedChanged);
            // 
            // checkBox18
            // 
            this.checkBox18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox18.AutoSize = true;
            this.checkBox18.Location = new System.Drawing.Point(372, 573);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(15, 14);
            this.checkBox18.TabIndex = 19;
            this.checkBox18.UseVisualStyleBackColor = true;
            this.checkBox18.CheckedChanged += new System.EventHandler(this.checkBox18_CheckedChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(7, 4);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(144, 16);
            this.label22.TabIndex = 2;
            this.label22.Text = "Auvergne-Rhône-Alpes";
            // 
            // label_ARA
            // 
            this.label_ARA.AutoSize = true;
            this.label_ARA.Location = new System.Drawing.Point(214, 4);
            this.label_ARA.Name = "label_ARA";
            this.label_ARA.Size = new System.Drawing.Size(42, 30);
            this.label_ARA.TabIndex = 0;
            this.label_ARA.Text = "label_ARA";
            // 
            // label_BFC
            // 
            this.label_BFC.AutoSize = true;
            this.label_BFC.Location = new System.Drawing.Point(214, 37);
            this.label_BFC.Name = "label_BFC";
            this.label_BFC.Size = new System.Drawing.Size(42, 30);
            this.label_BFC.TabIndex = 1;
            this.label_BFC.Text = "label_BFC";
            // 
            // label_Bre
            // 
            this.label_Bre.AutoSize = true;
            this.label_Bre.Location = new System.Drawing.Point(214, 70);
            this.label_Bre.Name = "label_Bre";
            this.label_Bre.Size = new System.Drawing.Size(42, 30);
            this.label_Bre.TabIndex = 2;
            this.label_Bre.Text = "label_Bre";
            // 
            // label_CVL
            // 
            this.label_CVL.AutoSize = true;
            this.label_CVL.Location = new System.Drawing.Point(214, 103);
            this.label_CVL.Name = "label_CVL";
            this.label_CVL.Size = new System.Drawing.Size(42, 30);
            this.label_CVL.TabIndex = 3;
            this.label_CVL.Text = "label_CVL";
            // 
            // label_Cor
            // 
            this.label_Cor.AutoSize = true;
            this.label_Cor.Location = new System.Drawing.Point(214, 136);
            this.label_Cor.Name = "label_Cor";
            this.label_Cor.Size = new System.Drawing.Size(42, 30);
            this.label_Cor.TabIndex = 4;
            this.label_Cor.Text = "label_Cor";
            // 
            // label_GrE
            // 
            this.label_GrE.AutoSize = true;
            this.label_GrE.Location = new System.Drawing.Point(214, 169);
            this.label_GrE.Name = "label_GrE";
            this.label_GrE.Size = new System.Drawing.Size(42, 30);
            this.label_GrE.TabIndex = 5;
            this.label_GrE.Text = "label_GrE";
            // 
            // label_HdF
            // 
            this.label_HdF.AutoSize = true;
            this.label_HdF.Location = new System.Drawing.Point(214, 202);
            this.label_HdF.Name = "label_HdF";
            this.label_HdF.Size = new System.Drawing.Size(42, 30);
            this.label_HdF.TabIndex = 6;
            this.label_HdF.Text = "label_HdF";
            // 
            // label_IdF
            // 
            this.label_IdF.AutoSize = true;
            this.label_IdF.Location = new System.Drawing.Point(214, 235);
            this.label_IdF.Name = "label_IdF";
            this.label_IdF.Size = new System.Drawing.Size(45, 30);
            this.label_IdF.TabIndex = 7;
            this.label_IdF.Text = "label_IdF";
            // 
            // label_Nor
            // 
            this.label_Nor.AutoSize = true;
            this.label_Nor.Location = new System.Drawing.Point(214, 268);
            this.label_Nor.Name = "label_Nor";
            this.label_Nor.Size = new System.Drawing.Size(42, 30);
            this.label_Nor.TabIndex = 8;
            this.label_Nor.Text = "label_Nor";
            // 
            // label_NAq
            // 
            this.label_NAq.AutoSize = true;
            this.label_NAq.Location = new System.Drawing.Point(214, 301);
            this.label_NAq.Name = "label_NAq";
            this.label_NAq.Size = new System.Drawing.Size(42, 30);
            this.label_NAq.TabIndex = 9;
            this.label_NAq.Text = "label_NAq";
            // 
            // label_Occ
            // 
            this.label_Occ.AutoSize = true;
            this.label_Occ.Location = new System.Drawing.Point(214, 334);
            this.label_Occ.Name = "label_Occ";
            this.label_Occ.Size = new System.Drawing.Size(42, 30);
            this.label_Occ.TabIndex = 10;
            this.label_Occ.Text = "label_Occ";
            // 
            // label_PdL
            // 
            this.label_PdL.AutoSize = true;
            this.label_PdL.Location = new System.Drawing.Point(214, 367);
            this.label_PdL.Name = "label_PdL";
            this.label_PdL.Size = new System.Drawing.Size(42, 30);
            this.label_PdL.TabIndex = 11;
            this.label_PdL.Text = "label_PdL";
            // 
            // label_PACA
            // 
            this.label_PACA.AutoSize = true;
            this.label_PACA.Location = new System.Drawing.Point(214, 400);
            this.label_PACA.Name = "label_PACA";
            this.label_PACA.Size = new System.Drawing.Size(42, 30);
            this.label_PACA.TabIndex = 12;
            this.label_PACA.Text = "label_PACA";
            // 
            // label_Gua
            // 
            this.label_Gua.AutoSize = true;
            this.label_Gua.Location = new System.Drawing.Point(214, 433);
            this.label_Gua.Name = "label_Gua";
            this.label_Gua.Size = new System.Drawing.Size(42, 30);
            this.label_Gua.TabIndex = 13;
            this.label_Gua.Text = "label_Gua";
            // 
            // label_Guy
            // 
            this.label_Guy.AutoSize = true;
            this.label_Guy.Location = new System.Drawing.Point(214, 466);
            this.label_Guy.Name = "label_Guy";
            this.label_Guy.Size = new System.Drawing.Size(42, 30);
            this.label_Guy.TabIndex = 14;
            this.label_Guy.Text = "label_Guy";
            // 
            // label_Mar
            // 
            this.label_Mar.AutoSize = true;
            this.label_Mar.Location = new System.Drawing.Point(214, 499);
            this.label_Mar.Name = "label_Mar";
            this.label_Mar.Size = new System.Drawing.Size(42, 30);
            this.label_Mar.TabIndex = 15;
            this.label_Mar.Text = "label_Mar";
            // 
            // label_Reu
            // 
            this.label_Reu.AutoSize = true;
            this.label_Reu.Location = new System.Drawing.Point(214, 532);
            this.label_Reu.Name = "label_Reu";
            this.label_Reu.Size = new System.Drawing.Size(42, 30);
            this.label_Reu.TabIndex = 16;
            this.label_Reu.Text = "label_Reu";
            // 
            // label_May
            // 
            this.label_May.AutoSize = true;
            this.label_May.Location = new System.Drawing.Point(214, 565);
            this.label_May.Name = "label_May";
            this.label_May.Size = new System.Drawing.Size(42, 30);
            this.label_May.TabIndex = 17;
            this.label_May.Text = "label_May";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 37);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(165, 16);
            this.label23.TabIndex = 18;
            this.label23.Text = "Bourgogne-Franche-Comté";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(7, 70);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(59, 16);
            this.label24.TabIndex = 19;
            this.label24.Text = "Bretagne";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 103);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(122, 16);
            this.label25.TabIndex = 20;
            this.label25.Text = "Centre-Val de Loire";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(7, 136);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(41, 16);
            this.label26.TabIndex = 21;
            this.label26.Text = "Corse";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(7, 169);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(65, 16);
            this.label27.TabIndex = 22;
            this.label27.Text = "Grand Est";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(7, 202);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(108, 16);
            this.label28.TabIndex = 23;
            this.label28.Text = "Hauts-de-France";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(7, 235);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(88, 16);
            this.label29.TabIndex = 24;
            this.label29.Text = "Île-de-France";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(7, 268);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(72, 16);
            this.label30.TabIndex = 25;
            this.label30.Text = "Normandie";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(7, 301);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(118, 16);
            this.label31.TabIndex = 26;
            this.label31.Text = "Nouvelle-Aquitaine";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(7, 334);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(60, 16);
            this.label32.TabIndex = 27;
            this.label32.Text = "Occitanie";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(7, 367);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(99, 16);
            this.label33.TabIndex = 28;
            this.label33.Text = "Pays de la Loire";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(7, 400);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(174, 16);
            this.label34.TabIndex = 29;
            this.label34.Text = "Provence-Alpes-Côte d\'Azur";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(7, 433);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(82, 16);
            this.label35.TabIndex = 30;
            this.label35.Text = "Guadeloupe ";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(7, 466);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(115, 16);
            this.label36.TabIndex = 31;
            this.label36.Text = "Guyane (française)";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(7, 499);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(73, 16);
            this.label37.TabIndex = 32;
            this.label37.Text = "Martinique ";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(7, 532);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(71, 16);
            this.label38.TabIndex = 33;
            this.label38.Text = "La Réunion";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(7, 565);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(53, 16);
            this.label39.TabIndex = 34;
            this.label39.Text = "Mayotte";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(109, -3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Liste des regions de France";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Nom de la région";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel2.Controls.Add(this.tableLayoutPanel2);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(465, 381);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(405, 174);
            this.panel2.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64.0625F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.9375F));
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label8, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label_reg_1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label_reg_2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label_reg_3, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label_rang_1, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label_rang_2, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label_rang_3, 1, 3);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 40);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.38095F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(384, 126);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Nom de la région";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(249, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Rang";
            // 
            // label_reg_1
            // 
            this.label_reg_1.AutoSize = true;
            this.label_reg_1.Location = new System.Drawing.Point(6, 35);
            this.label_reg_1.Name = "label_reg_1";
            this.label_reg_1.Size = new System.Drawing.Size(65, 13);
            this.label_reg_1.TabIndex = 6;
            this.label_reg_1.Text = "Non attribué";
            // 
            // label_reg_2
            // 
            this.label_reg_2.AutoSize = true;
            this.label_reg_2.Location = new System.Drawing.Point(6, 63);
            this.label_reg_2.Name = "label_reg_2";
            this.label_reg_2.Size = new System.Drawing.Size(65, 13);
            this.label_reg_2.TabIndex = 7;
            this.label_reg_2.Text = "Non attribué";
            // 
            // label_reg_3
            // 
            this.label_reg_3.AutoSize = true;
            this.label_reg_3.Location = new System.Drawing.Point(6, 94);
            this.label_reg_3.Name = "label_reg_3";
            this.label_reg_3.Size = new System.Drawing.Size(65, 13);
            this.label_reg_3.TabIndex = 8;
            this.label_reg_3.Text = "Non attribué";
            // 
            // label_rang_1
            // 
            this.label_rang_1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_rang_1.AutoSize = true;
            this.label_rang_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_rang_1.Location = new System.Drawing.Point(304, 37);
            this.label_rang_1.Name = "label_rang_1";
            this.label_rang_1.Size = new System.Drawing.Size(18, 20);
            this.label_rang_1.TabIndex = 9;
            this.label_rang_1.Text = "1";
            // 
            // label_rang_2
            // 
            this.label_rang_2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_rang_2.AutoSize = true;
            this.label_rang_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_rang_2.Location = new System.Drawing.Point(304, 67);
            this.label_rang_2.Name = "label_rang_2";
            this.label_rang_2.Size = new System.Drawing.Size(18, 20);
            this.label_rang_2.TabIndex = 10;
            this.label_rang_2.Text = "2";
            // 
            // label_rang_3
            // 
            this.label_rang_3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_rang_3.AutoSize = true;
            this.label_rang_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_rang_3.Location = new System.Drawing.Point(304, 98);
            this.label_rang_3.Name = "label_rang_3";
            this.label_rang_3.Size = new System.Drawing.Size(18, 20);
            this.label_rang_3.TabIndex = 11;
            this.label_rang_3.Text = "3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(87, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "Voeux souhaités";
            // 
            // label_nom_prenom
            // 
            this.label_nom_prenom.AutoSize = true;
            this.label_nom_prenom.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_nom_prenom.Location = new System.Drawing.Point(478, 84);
            this.label_nom_prenom.Name = "label_nom_prenom";
            this.label_nom_prenom.Size = new System.Drawing.Size(64, 25);
            this.label_nom_prenom.TabIndex = 2;
            this.label_nom_prenom.Text = "label3";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel3.Controls.Add(this.label40);
            this.panel3.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(12, 12);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(860, 55);
            this.panel3.TabIndex = 6;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label40.Location = new System.Drawing.Point(248, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(410, 46);
            this.label40.TabIndex = 0;
            this.label40.Text = "Galaxy Swiss Bourdin";
            // 
            // btn_valider
            // 
            this.btn_valider.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btn_valider.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_valider.Font = new System.Drawing.Font("Liberation Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_valider.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_valider.Location = new System.Drawing.Point(464, 568);
            this.btn_valider.Name = "btn_valider";
            this.btn_valider.Size = new System.Drawing.Size(100, 30);
            this.btn_valider.TabIndex = 18;
            this.btn_valider.Text = "Valider";
            this.btn_valider.UseVisualStyleBackColor = false;
            this.btn_valider.Click += new System.EventHandler(this.btn_valider_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel4.Controls.Add(this.button3);
            this.panel4.Controls.Add(this.btn_deconnexion);
            this.panel4.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel4.Location = new System.Drawing.Point(465, 617);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(407, 100);
            this.panel4.TabIndex = 21;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.Control;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.Font = new System.Drawing.Font("Liberation Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button3.Location = new System.Drawing.Point(290, 59);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 30);
            this.button3.TabIndex = 29;
            this.button3.Text = "Quitter";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_deconnexion
            // 
            this.btn_deconnexion.BackColor = System.Drawing.SystemColors.Control;
            this.btn_deconnexion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_deconnexion.Font = new System.Drawing.Font("Liberation Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_deconnexion.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_deconnexion.Location = new System.Drawing.Point(15, 59);
            this.btn_deconnexion.Name = "btn_deconnexion";
            this.btn_deconnexion.Size = new System.Drawing.Size(123, 30);
            this.btn_deconnexion.TabIndex = 27;
            this.btn_deconnexion.Text = "Déconnexion";
            this.btn_deconnexion.UseVisualStyleBackColor = false;
            this.btn_deconnexion.Click += new System.EventHandler(this.btn_deconnexion_Click);
            // 
            // button_modifier
            // 
            this.button_modifier.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button_modifier.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_modifier.Font = new System.Drawing.Font("Liberation Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_modifier.ForeColor = System.Drawing.SystemColors.Control;
            this.button_modifier.Location = new System.Drawing.Point(465, 331);
            this.button_modifier.Name = "button_modifier";
            this.button_modifier.Size = new System.Drawing.Size(100, 30);
            this.button_modifier.TabIndex = 22;
            this.button_modifier.Text = "Modifier";
            this.button_modifier.UseVisualStyleBackColor = false;
            this.button_modifier.Click += new System.EventHandler(this.button_modifier_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel5.Controls.Add(this.tableLayoutPanel3);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Location = new System.Drawing.Point(465, 114);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(405, 174);
            this.panel5.TabIndex = 23;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64.0625F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.9375F));
            this.tableLayoutPanel3.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label9, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label10, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label11, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label12, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label13, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label14, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label15, 1, 3);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(6, 40);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.38095F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(384, 126);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Nom de la région";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(249, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Rang";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 35);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Non attribué";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 63);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Non attribué";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 94);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Non attribué";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(304, 37);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 20);
            this.label13.TabIndex = 9;
            this.label13.Text = "1";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(304, 67);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(18, 20);
            this.label14.TabIndex = 10;
            this.label14.Text = "2";
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(304, 98);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(18, 20);
            this.label15.TabIndex = 11;
            this.label15.Text = "3";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(87, 12);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(209, 18);
            this.label16.TabIndex = 0;
            this.label16.Text = "Voeux enregistrés à votre nom";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(468, 296);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "label17";
            this.label17.Visible = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Font = new System.Drawing.Font("Liberation Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.Control;
            this.button1.Location = new System.Drawing.Point(584, 331);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(177, 30);
            this.button1.TabIndex = 25;
            this.button1.Text = "Valider la modification";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_annuler
            // 
            this.btn_annuler.BackColor = System.Drawing.Color.DarkRed;
            this.btn_annuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_annuler.Font = new System.Drawing.Font("Liberation Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_annuler.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_annuler.Location = new System.Drawing.Point(770, 331);
            this.btn_annuler.Name = "btn_annuler";
            this.btn_annuler.Size = new System.Drawing.Size(100, 30);
            this.btn_annuler.TabIndex = 26;
            this.btn_annuler.Text = "Annuler";
            this.btn_annuler.UseVisualStyleBackColor = false;
            this.btn_annuler.Click += new System.EventHandler(this.btn_annuler_Click);
            // 
            // btn_annul_dem_voeux
            // 
            this.btn_annul_dem_voeux.BackColor = System.Drawing.Color.DarkRed;
            this.btn_annul_dem_voeux.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_annul_dem_voeux.Font = new System.Drawing.Font("Liberation Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_annul_dem_voeux.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_annul_dem_voeux.Location = new System.Drawing.Point(695, 568);
            this.btn_annul_dem_voeux.Name = "btn_annul_dem_voeux";
            this.btn_annul_dem_voeux.Size = new System.Drawing.Size(177, 30);
            this.btn_annul_dem_voeux.TabIndex = 27;
            this.btn_annul_dem_voeux.Text = "Annuler la demande";
            this.btn_annul_dem_voeux.UseVisualStyleBackColor = false;
            this.btn_annul_dem_voeux.Click += new System.EventHandler(this.btn_annul_dem_voeux_Click);
            // 
            // btn_annuler_2
            // 
            this.btn_annuler_2.BackColor = System.Drawing.Color.DarkRed;
            this.btn_annuler_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_annuler_2.Font = new System.Drawing.Font("Liberation Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_annuler_2.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_annuler_2.Location = new System.Drawing.Point(584, 568);
            this.btn_annuler_2.Name = "btn_annuler_2";
            this.btn_annuler_2.Size = new System.Drawing.Size(100, 30);
            this.btn_annuler_2.TabIndex = 28;
            this.btn_annuler_2.Text = "Annuler";
            this.btn_annuler_2.UseVisualStyleBackColor = false;
            this.btn_annuler_2.Click += new System.EventHandler(this.btn_annuler_2_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(884, 729);
            this.Controls.Add(this.btn_annuler_2);
            this.Controls.Add(this.btn_annul_dem_voeux);
            this.Controls.Add(this.btn_annuler);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.button_modifier);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.btn_valider);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label_nom_prenom);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_nom_prenom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label_ARA;
        private System.Windows.Forms.Label label_BFC;
        private System.Windows.Forms.Label label_Bre;
        private System.Windows.Forms.Label label_CVL;
        private System.Windows.Forms.Label label_Cor;
        private System.Windows.Forms.Label label_GrE;
        private System.Windows.Forms.Label label_HdF;
        private System.Windows.Forms.Label label_IdF;
        private System.Windows.Forms.Label label_Nor;
        private System.Windows.Forms.Label label_NAq;
        private System.Windows.Forms.Label label_Occ;
        private System.Windows.Forms.Label label_PdL;
        private System.Windows.Forms.Label label_PACA;
        private System.Windows.Forms.Label label_Gua;
        private System.Windows.Forms.Label label_Guy;
        private System.Windows.Forms.Label label_Mar;
        private System.Windows.Forms.Label label_Reu;
        private System.Windows.Forms.Label label_May;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ComboBox comboBox18;
        private System.Windows.Forms.ComboBox comboBox17;
        private System.Windows.Forms.ComboBox comboBox16;
        private System.Windows.Forms.ComboBox comboBox15;
        private System.Windows.Forms.ComboBox comboBox14;
        private System.Windows.Forms.ComboBox comboBox13;
        private System.Windows.Forms.ComboBox comboBox12;
        private System.Windows.Forms.ComboBox comboBox11;
        private System.Windows.Forms.ComboBox comboBox10;
        private System.Windows.Forms.ComboBox comboBox9;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.Button btn_valider;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label_reg_1;
        private System.Windows.Forms.Label label_reg_2;
        private System.Windows.Forms.Label label_reg_3;
        private System.Windows.Forms.Label label_rang_1;
        private System.Windows.Forms.Label label_rang_2;
        private System.Windows.Forms.Label label_rang_3;
        private System.Windows.Forms.Button button_modifier;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_annuler;
        private System.Windows.Forms.Button btn_annul_dem_voeux;
        private System.Windows.Forms.Button btn_annuler_2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btn_deconnexion;
    }
}