﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPE_4_CS
{
    public partial class ChoixVoeux : Form
    {
        
        ClassConnexion obj_co = new ClassConnexion();
        FormConnexion form_1 = new FormConnexion();

        public ChoixVoeux()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {
            try
            {
                obj_co.Connexion();

                //Initialise a 0 le compteur de région choisies. Si le compteur devient superieur à 3 la validation devient impossible et un message d'avertissement s'affiche
                Class_utilisateur.Count = 0;

                if (Class_utilisateur.Acceptation == 1)
                {
                    string voeu_accepte = obj_co.Select_text("SELECT `ordre` FROM `voeux` WHERE `etat` = 'Accepté' and `id_utilisateur` = " + Class_utilisateur.Id + "");
                    string region_acc = obj_co.Select_text("SELECT `nom` FROM `region` JOIN `voeux` ON `region`.`id_Region` = `voeux`.`Id_Region_CHOISIR` WHERE `ordre` = " + voeu_accepte + " AND `id_utilisateur` = '" + Class_utilisateur.Id + "'");

                    MessageBox.Show("Félicitations, le voeu n°" + voeu_accepte + " dans la région " + region_acc + " a été accepté.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    
                    button_modifier.Enabled = false;
                    btn_annuler_2.Enabled = false;
                    btn_valider.Enabled = false;
                }
                else if (Class_utilisateur.Acceptation == 0)
                {
                    MessageBox.Show("Bonjour,\n\nMalheureusement votre demande de voeux a été refusée.\nVous pouvez toujours refaire une demande de voeux qui examinée par notre équipe.\nNous restons à votre disposition pour toutes informations\n\nCordialement\n\nLa direction des Resources Humaines", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {

                }

                //Inscrit le nom ete le prénom de l'utilisateur de la session actuelle
                label_nom_prenom.Text = "Bonjour " + Class_utilisateur.Prenom + " " + Class_utilisateur.Nom;

                //Ecrit le nom de chaque région choisie en cas de demande déjà faite ou alors "non atribué" en cas d'absence de demande de voeux
                label10.Text = Class_utilisateur.Reg_1;
                label11.Text = Class_utilisateur.Reg_2;
                label12.Text = Class_utilisateur.Reg_3;

                //Affiche un message concernant la situation actuelle de l'utilisateur
                label17.Visible = true;
                label17.Text = Class_utilisateur.Ordre;

                //Regarde le statut actuel du booléen de l'utilisateur
                //Si l'utilisateur à déjà une demande de voeux en cours, désactive toutes les checkbox et le panel de nouvelle demande de voeux
                if (Class_utilisateur.Panel == true)
                {
                    panel2.Enabled = false;
                    Class_utilisateur.Procedure_Enabled_checkbox(tableLayoutPanel1, true);
                }
                //Si l'utilisateur n'a pas de demande de voeux, active toutes les checkbox et active le panel de nouvelle demande
                else
                {
                    panel5.Enabled = false;
                    panel2.Enabled = true;
                    btn_annul_dem_voeux.Enabled = false;
                    button_modifier.Enabled = false;
                    btn_annuler.Enabled = false;
                    button1.Enabled = false;
                    Class_utilisateur.Procedure_Enabled_checkbox(tableLayoutPanel1, false);
                }

                //Ecrit le nombre de places disponibles pour chaque région
                label_ARA.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 1");
                label_BFC.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 2");
                label_Bre.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 3");
                label_CVL.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 4");
                label_Cor.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 5");
                label_GrE.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 6");
                label_HdF.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 7");
                label_IdF.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 8");
                label_Nor.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 9");
                label_NAq.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 10");
                label_Occ.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 11");
                label_PdL.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 12");
                label_PACA.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 13");
                label_Gua.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 14");
                label_Guy.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 15");
                label_Mar.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 16");
                label_Reu.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 17");
                label_May.Text = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `id_Region` = 18");

                //Désactive chaque combobox du tableLayoutPanel et insert dans chaque combobox le choix de priorité disponible pour chaque région
                Class_utilisateur.Procedure_Enabled(tableLayoutPanel1);

                btn_annuler.Enabled = false;
                button1.Enabled = false;
            }
            catch (Exception ex)
            {
                //Message en cas d'erreur lors de l'execution des requetes de mis a jour des voeux
                MessageBox.Show("Erreur : " + ex, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            

        }              

        //18 checkbox, 1 pour chaque région
        //Procedure de liaison entre chaque checkbox, leur combobox respectifs et le bouton validé en cas de plus de 3 voeux
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox1, btn_valider, comboBox1);
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox2, btn_valider, comboBox2);
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox3, btn_valider, comboBox3);
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox4, btn_valider, comboBox4);
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox5, btn_valider, comboBox5);
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox6, btn_valider, comboBox6);
        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox7, btn_valider, comboBox7);
        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox8, btn_valider, comboBox8);
        }

        private void checkBox9_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox9, btn_valider, comboBox9);
        }

        private void checkBox10_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox10, btn_valider, comboBox10);
        }

        private void checkBox11_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox11, btn_valider, comboBox11);
        }

        private void checkBox12_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox12, btn_valider, comboBox12);
        }

        private void checkBox13_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox13, btn_valider, comboBox13);
        }

        private void checkBox14_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox14, btn_valider, comboBox14);
        }

        private void checkBox15_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox15, btn_valider, comboBox15);
        }

        private void checkBox16_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox16, btn_valider, comboBox16);
        }

        private void checkBox17_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox17, btn_valider, comboBox17);
        }

        private void checkBox18_CheckedChanged(object sender, EventArgs e)
        {
            Class_utilisateur.Procedure_CheckBox(checkBox18, btn_valider, comboBox18);
        }

        //18 combobox, 1 pour chaque région
        //Procédure pour chaque combobox qui récupère la priorité et le nom de la region par rapport a l'id de la region correspondant au numero du combobox 
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox1, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox1, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox2, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox2, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox3, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox3, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox4, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox4, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox5, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox5, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox6, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox6, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void comboBox7_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox7, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox7, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void comboBox8_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox8, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox8, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void comboBox9_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox9, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox9, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void comboBox10_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox10, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox10, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void comboBox11_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox11, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox11, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void comboBox12_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox12, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox12, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void comboBox13_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox13, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox13, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void comboBox14_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox14, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox14, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void comboBox15_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox15, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox15, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void comboBox16_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox16, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox16, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void comboBox17_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox17, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox17, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void comboBox18_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Class_utilisateur.Panel == true)
            {
                Class_utilisateur.Procedure_ordre(comboBox18, label10, label11, label12);
            }
            else
            {
                Class_utilisateur.Procedure_ordre(comboBox18, label_reg_1, label_reg_2, label_reg_3);
            }
        }

        private void btn_valider_Click(object sender, EventArgs e)
        {
            try
            {
                string reg_1 = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `nom` = '" + label_reg_1.Text + "'");
                string reg_2 = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `nom` = '" + label_reg_2.Text + "'");
                string reg_3 = obj_co.Select_text("SELECT `nbPlace` FROM `region` WHERE `nom` = '" + label_reg_3.Text + "'");

                //Regarde les labels de régions déjà choisies pour savoir si l'utilisateur n'a pas de demande de voeux en cours
                if (label10.Text == "Non attribué" || label11.Text == "Non attribué" || label12.Text == "Non attribué")
                {
                    //Regarde si l'utilisateur n'a pas rempli ses 3 voeux
                    if (label_reg_1.Text == "Non attribué" || label_reg_2.Text == "Non attribué" || label_reg_3.Text == "Non attribué")
                    {
                        MessageBox.Show("Un ou plusieurs voeux n'ont pas été rempli.", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    //Regarde si une region apparait 2 fois ou plus dans les choix de voeux de l'utilisateur
                    else if (label_reg_1.Text == label_reg_2.Text || label_reg_1.Text == label_reg_3.Text || label_reg_2.Text == label_reg_3.Text)
                    {
                        MessageBox.Show("Il y a 2 regions identiques dans vos choix.", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else if (reg_1 == "0" || reg_2 == "0" || reg_3 == "0")
                    {
                        MessageBox.Show("Vous avez choisi une région ne comportant aucune place disponible.\n\nVeuillez changer votre choix.", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    //Après vérification, autorise l'utilisateur à faire une demande de voeux
                    else
                    {
                        //Requetes d'insertion des voeux de l'utilisateur dans la table "voeux" de la base de données
                        string voeux_1 = "INSERT INTO `voeux`(`id_utilisateur`, `Ordre`, `etat`, `DateCreation`, `id_Region_CHOISIR`) VALUES ('" + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `login` = '" + Class_utilisateur.Login + "'") + "', '1', 'Nouveau', '" + DateTime.Now.ToString("yyyy/MM/dd") + "', '" + obj_co.Select_text("SELECT `id_Region` FROM `region` WHERE `nom`= '" + label_reg_1.Text + "'") + "')";
                        string voeux_2 = "INSERT INTO `voeux`(`id_utilisateur`, `Ordre`, `etat`, `DateCreation`, `id_Region_CHOISIR`) VALUES ('" + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `login` = '" + Class_utilisateur.Login + "'") + "', '2', 'Nouveau', '" + DateTime.Now.ToString("yyyy/MM/dd") + "', '" + obj_co.Select_text("SELECT `id_Region` FROM `region` WHERE `nom`= '" + label_reg_2.Text + "'") + "')";
                        string voeux_3 = "INSERT INTO `voeux`(`id_utilisateur`, `Ordre`, `etat`, `DateCreation`, `id_Region_CHOISIR`) VALUES ('" + obj_co.Select_text("SELECT `id_utilisateur` FROM `utilisateur` WHERE `login` = '" + Class_utilisateur.Login + "'") + "', '3', 'Nouveau', '" + DateTime.Now.ToString("yyyy/MM/dd") + "', '" + obj_co.Select_text("SELECT `id_Region` FROM `region` WHERE `nom`= '" + label_reg_3.Text + "'") + "')";

                        string rappel = "\n\nRappel :\n\nVos voeux sont : \n\nVoeu 1 : " + label_reg_1.Text + "\nVoeu 2 : " + label_reg_2.Text + "\nVoeu 3 : " + label_reg_3.Text + "";

                        DialogResult rep_utilisateur;

                        //Message de rappel des voeux de l'utilisateur avant validation
                        rep_utilisateur = MessageBox.Show("Vos voeux vont être ajoutés à la base, voulez vous continuer ?" + rappel, "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                        //Récuperation de la réponse de l'utilisateur
                        if (rep_utilisateur == DialogResult.OK)
                        {
                            //Execute les requetes d'insertion des voeux dans la table "voeux" de la base de données
                            obj_co.Execute_requete(voeux_1);
                            obj_co.Execute_requete(voeux_2);
                            obj_co.Execute_requete(voeux_3);

                            //Récupère l'id de chaque voeux demandé par l'utilisateur pour l'insertion dans la table "demande_d_un_voeux" de la base de données
                            string recup_voeu_1 = obj_co.Select_text("SELECT `id_Voeux` FROM `Voeux` WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 1");
                            string recup_voeu_2 = obj_co.Select_text("SELECT `id_Voeux` FROM `Voeux` WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 2");
                            string recup_voeu_3 = obj_co.Select_text("SELECT `id_Voeux` FROM `Voeux` WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 3");

                            //Insert l'id de chaque voeux ainsi que l'id de l'utilisateur dans la table "demande_d_un_voeux"
                            string dem_voeux_1 = "INSERT INTO `demande_d_un_voeux` (`id_utilisateur`, `id_Voeux`) VALUES (" + Class_utilisateur.Id + ", " + recup_voeu_1 + ")";
                            string dem_voeux_2 = "INSERT INTO `demande_d_un_voeux` (`id_utilisateur`, `id_Voeux`) VALUES (" + Class_utilisateur.Id + ", " + recup_voeu_2 + ")";
                            string dem_voeux_3 = "INSERT INTO `demande_d_un_voeux` (`id_utilisateur`, `id_Voeux`) VALUES (" + Class_utilisateur.Id + ", " + recup_voeu_3 + ")";

                            //Execute les requetes des voeux choisis par l'utilisateur
                            obj_co.Execute_requete(dem_voeux_1);
                            obj_co.Execute_requete(dem_voeux_2);
                            obj_co.Execute_requete(dem_voeux_3);

                            MessageBox.Show("Vos voeux ont bien été enregistrés.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            //Désactive le panel de nouvelle demande de voeux
                            panel2.Enabled = false;

                            //Désactive toutes les checkbox du tableLayoutPanel
                            Class_utilisateur.Procedure_Enabled_checkbox(tableLayoutPanel1, true);

                            //Passe le booléen de demande de voeux a true suite à la réalisation d'une demande de voeux
                            Class_utilisateur.Panel = true;

                            //Active les controles correspondant à une demande de voeux déjà en cours
                            panel5.Enabled = true;
                            btn_annul_dem_voeux.Enabled = true;
                            button_modifier.Enabled = true;
                            btn_annuler.Enabled = true;
                            button1.Enabled = true;

                            //Désactive tous les contrôles correspondant à une nouvelle demande
                            btn_valider.Enabled = false;
                            btn_annuler_2.Enabled = false;

                            //Affiche les regions choisies dans les labels de demande de voeux en cours
                            label10.Text = Class_utilisateur.Reg_1 = obj_co.Select_text("SELECT `nom` FROM `region` JOIN `voeux` ON region.id_Region = voeux.id_Region_CHOISIR  WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 1 ");
                            label11.Text = Class_utilisateur.Reg_2 = obj_co.Select_text("SELECT `nom` FROM `region` JOIN `voeux` ON region.id_Region = voeux.id_Region_CHOISIR  WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 2 ");
                            label12.Text = Class_utilisateur.Reg_3 = obj_co.Select_text("SELECT `nom` FROM `region` JOIN `voeux` ON region.id_Region = voeux.id_Region_CHOISIR  WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 3 ");

                            //Remet les labels région de nouvelle demande par défaut
                            label_reg_1.Text = "Non attribué";
                            label_reg_2.Text = "Non attribué";
                            label_reg_3.Text = "Non attribué";

                            //Affiche un message d'instruction
                            Class_utilisateur.Ordre = "Vous avez déjà une demande de voeux actuelle en cours à votre nom.\nVous pouvez la modifier si vous le souhaitez.";
                            label17.Text = Class_utilisateur.Ordre;

                            // Cherche les combobox dont le texte n'est pas vide et efface ce texte
                            foreach (ComboBox cbbx in tableLayoutPanel1.Controls.OfType<ComboBox>())
                            {
                                if (cbbx.SelectedIndex == 0 || cbbx.SelectedIndex == 1 || cbbx.SelectedIndex == 2)
                                {
                                    cbbx.Text = null;
                                }
                            }

                            // Cherche les checkbox cochées et les decoches
                            foreach (CheckBox cbx in tableLayoutPanel1.Controls.OfType<CheckBox>())
                            {
                                if (cbx.Checked == true)
                                {
                                    cbx.Checked = false;
                                }
                            }
                        }
                        else
                        {

                        }
                    }

                }
                else
                {
                    //Message d'avertissement en cas de demande de voeux déjà réalisée
                    MessageBox.Show("Vous avez deja fait une demande de voeux.\nVous pouvez modifier vos voeux actuels mais vous ne pouvez pas faire de nouvelle demande.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                //Message en cas d'erreur lors de l'execution des requetes de mis a jour des voeux
                MessageBox.Show("Erreur : " + ex, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            

        }

        private void button_modifier_Click(object sender, EventArgs e)
        {
            try
            {
                //Active toutes les checkbox du tableLayoutPanel
                Class_utilisateur.Procedure_Enabled_checkbox(tableLayoutPanel1, false);
                btn_annuler.Enabled = true;
                button1.Enabled = true;
            }
            catch (Exception ex)
            {
                //Message en cas d'erreur lors de l'execution des requetes de mis a jour des voeux
                MessageBox.Show("Erreur : " + ex, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    //Récupère les id des voeux réalisés au départ par l'utilisateur actuel
                    Class_utilisateur.Mod_voeu_1 = obj_co.Select_text("SELECT `id_Voeux` FROM `voeux` WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 1");
                    Class_utilisateur.Mod_voeu_2 = obj_co.Select_text("SELECT `id_Voeux` FROM `voeux` WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 2");
                    Class_utilisateur.Mod_voeu_3 = obj_co.Select_text("SELECT `id_Voeux` FROM `voeux` WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 3");
                }
                catch (Exception ex)
                {
                    //Message en cas d'erreur lors de l'execution des requetes de mis a jour des voeux
                    MessageBox.Show("Erreur : " + ex, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


                //Requetes de mis a jour de l'ordre et de la region de chaque voeux
                string voeux_1 = "UPDATE `voeux` SET `Ordre` = '" + label13.Text + "', `id_Region_CHOISIR` = '" + obj_co.Select_text("SELECT `id_Region` FROM `region` WHERE `nom`= '" + label10.Text + "'") + "' WHERE `voeux`.`id_Voeux` = " + Class_utilisateur.Mod_voeu_1 + " ";
                string voeux_2 = "UPDATE `voeux` SET `Ordre` = '" + label14.Text + "', `id_Region_CHOISIR` = '" + obj_co.Select_text("SELECT `id_Region` FROM `region` WHERE `nom`= '" + label11.Text + "'") + "' WHERE `voeux`.`id_Voeux` = " + Class_utilisateur.Mod_voeu_2 + " ";
                string voeux_3 = "UPDATE `voeux` SET `Ordre` = '" + label15.Text + "', `id_Region_CHOISIR` = '" + obj_co.Select_text("SELECT `id_Region` FROM `region` WHERE `nom`= '" + label12.Text + "'") + "' WHERE `voeux`.`id_Voeux` = " + Class_utilisateur.Mod_voeu_3 + " ";

                try
                {
                    if (label10.Text == label11.Text || label10.Text == label12.Text || label11.Text == label12.Text)
                    {
                        MessageBox.Show("Il y a 2 regions identiques dans vos choix.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        string rappel = "\n\nRappel :\n\nVos voeux sont : \n\nVoeu 1 : " + label10.Text + "\nVoeu 2 : " + label11.Text + "\nVoeu 3 : " + label12.Text + "";

                        //Message de rappel de la modification des voeux avant confirmation
                        DialogResult rep_utilisateur;
                        rep_utilisateur = MessageBox.Show("Vos voeux vont être ajoutés à la base, voulez vous continuer ?" + rappel, "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                        //Récuperation de la réponse de l'utilisateur
                        if (rep_utilisateur == DialogResult.OK)
                        {
                            //Execution des requetes de mis a jour des voeux de l'utilisateur
                            obj_co.Execute_requete(voeux_1);
                            obj_co.Execute_requete(voeux_2);
                            obj_co.Execute_requete(voeux_3);

                            //Message de confirmation de la modification
                            MessageBox.Show("Vos voeux ont bien été modifiés.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            btn_annuler.Enabled = false;
                        }
                    }

                    button1.Enabled = false;


                }
                catch (Exception ex)
                {
                    //Message en cas d'erreur lors de l'execution des requetes de mis a jour des voeux
                    MessageBox.Show("Erreur : " + ex, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                //Message en cas d'erreur lors de l'execution des requetes de mis a jour des voeux
                MessageBox.Show("Erreur : " + ex, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            
        }

        private void btn_annuler_Click(object sender, EventArgs e)
        {
            try
            {
                //Grise toutes les checkbox du tableLayoutPanel
                Class_utilisateur.Procedure_Enabled_checkbox(tableLayoutPanel1, true);

                //Annule le changement de nom des regions dans le panel
                label10.Text = Class_utilisateur.Reg_1;
                label11.Text = Class_utilisateur.Reg_2;
                label12.Text = Class_utilisateur.Reg_3;

                // Cherche les combobox dont le texte n'est pas vide et efface ce texte
                foreach (ComboBox cbbx in tableLayoutPanel1.Controls.OfType<ComboBox>())
                {
                    if (cbbx.SelectedIndex == 0 || cbbx.SelectedIndex == 1 || cbbx.SelectedIndex == 2)
                    {
                        cbbx.Text = null;
                    }
                }

                // Cherche les checkbox cochées et les decoches
                foreach (CheckBox cbx in tableLayoutPanel1.Controls.OfType<CheckBox>())
                {
                    if (cbx.Checked == true)
                    {
                        cbx.Checked = false;
                    }
                }

                btn_annuler.Enabled = false;
                button1.Enabled = false;
            }
            catch (Exception ex)
            {
                //Message en cas d'erreur lors de l'execution des requetes de mis a jour des voeux
                MessageBox.Show("Erreur : " + ex, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Ferme l'application
            obj_co.Deconnexion();

            this.Close();
            this.Dispose();
            form_1.Close();
            form_1.Dispose();
            
        }

        private void btn_deconnexion_Click(object sender, EventArgs e)
        {
            try
            {
                //Déconnecte l'utilisateur et retourne au formulaire de connexion
                Class_utilisateur.Id = Convert.ToInt32(null);
                Class_utilisateur.Nom = "";
                Class_utilisateur.Prenom = "";
                Class_utilisateur.Reg_1 = "";
                Class_utilisateur.Reg_2 = "";
                Class_utilisateur.Reg_3 = "";
                Class_utilisateur.Login = "";

                obj_co.Deconnexion();
                form_1.Show();
                this.Close();
                this.Dispose();
            }
            catch (Exception ex)
            {
                //Message en cas d'erreur lors de l'execution des requetes de mis a jour des voeux
                MessageBox.Show("Erreur : " + ex, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btn_annul_dem_voeux_Click(object sender, EventArgs e)
        {
            try
            {
                string suppr_voeu_1 = "DELETE FROM `voeux` WHERE `voeux`.`id_utilisateur` = " + Class_utilisateur.Id + " AND `voeux`.`ordre` = 1;";
                string suppr_voeu_2 = "DELETE FROM `voeux` WHERE `voeux`.`id_utilisateur` = " + Class_utilisateur.Id + " AND `voeux`.`ordre` = 2;";
                string suppr_voeu_3 = "DELETE FROM `voeux` WHERE `voeux`.`id_utilisateur` = " + Class_utilisateur.Id + " AND `voeux`.`ordre` = 3;";

                string sup_dem_voeu_1 = "DELETE FROM `demande_d_un_voeux` WHERE `demande_d_un_voeux`.`id_utilisateur` = " + Class_utilisateur.Id + " AND `demande_d_un_voeux`.`id_Voeux` = " + obj_co.Select_text("SELECT `id_Voeux` FROM `Voeux` WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 1") + ";";
                string sup_dem_voeu_2 = "DELETE FROM `demande_d_un_voeux` WHERE `demande_d_un_voeux`.`id_utilisateur` = " + Class_utilisateur.Id + " AND `demande_d_un_voeux`.`id_Voeux` = " + obj_co.Select_text("SELECT `id_Voeux` FROM `Voeux` WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 2") + ";";
                string sup_dem_voeu_3 = "DELETE FROM `demande_d_un_voeux` WHERE `demande_d_un_voeux`.`id_utilisateur` = " + Class_utilisateur.Id + " AND `demande_d_un_voeux`.`id_Voeux` = " + obj_co.Select_text("SELECT `id_Voeux` FROM `Voeux` WHERE `id_utilisateur` = " + Class_utilisateur.Id + " AND `Ordre` = 3") + ";";

                try
                {
                    DialogResult rep_utilisateur;
                    rep_utilisateur = MessageBox.Show("Votre demande de voeux va être supprimée de notre base, voulez vous continuer ?", "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                    if (rep_utilisateur == DialogResult.OK)
                    {
                        obj_co.Execute_requete(sup_dem_voeu_1);
                        obj_co.Execute_requete(sup_dem_voeu_2);
                        obj_co.Execute_requete(sup_dem_voeu_3);

                        obj_co.Execute_requete(suppr_voeu_1);
                        obj_co.Execute_requete(suppr_voeu_2);
                        obj_co.Execute_requete(suppr_voeu_3);

                        MessageBox.Show("Vos voeux ont bien été supprimés.\nVous pouvez maintenant faire une nouvelle demande.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        panel2.Enabled = true;
                        btn_annuler_2.Enabled = true;
                        panel5.Enabled = false;
                        btn_annul_dem_voeux.Enabled = false;
                        button_modifier.Enabled = false;
                        btn_annuler.Enabled = false;
                        button1.Enabled = false;

                        label10.Text = "Non attribué";
                        label11.Text = "Non attribué";
                        label12.Text = "Non attribué";

                        Class_utilisateur.Panel = false;

                        Class_utilisateur.Ordre = "Vous n'avez pas de demande de voeux actuelle.\nChoisissez des regions et leur priorité et cliquez sur valider.";
                        label17.Text = Class_utilisateur.Ordre;

                        Class_utilisateur.Procedure_Enabled_checkbox(tableLayoutPanel1, false);

                        btn_annul_dem_voeux.Enabled = false;

                    }
                }
                catch (Exception ex)
                {
                    //Message en cas d'erreur lors de l'execution des requetes de mis a jour des voeux
                    MessageBox.Show("Erreur : " + ex, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                //Message en cas d'erreur lors de l'execution des requetes de mis a jour des voeux
                MessageBox.Show("Erreur : " + ex, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btn_annuler_2_Click(object sender, EventArgs e)
        {
            try
            {
                //Annule le changement de nom des regions dans le panel
                label_reg_1.Text = "Non attribué";
                label_reg_2.Text = "Non attribué";
                label_reg_3.Text = "Non attribué";

                // Cherche les combobox dont le texte n'est pas vide et efface ce texte
                foreach (ComboBox cbbx in tableLayoutPanel1.Controls.OfType<ComboBox>())
                {
                    if (cbbx.SelectedIndex == 0 || cbbx.SelectedIndex == 1 || cbbx.SelectedIndex == 2)
                    {
                        cbbx.Text = null;
                    }
                }

                // Cherche les checkbox cochées et les decoches
                foreach (CheckBox cbx in tableLayoutPanel1.Controls.OfType<CheckBox>())
                {
                    if (cbx.Checked == true)
                    {
                        cbx.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                //Message en cas d'erreur lors de l'execution des requetes de mis a jour des voeux
                MessageBox.Show("Erreur : " + ex, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }
    }
}
